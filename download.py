import os

import requests

from bs4 import BeautifulSoup


def get_html_page(url):
    response = requests.get(url)

    return response.text


def get_album_info(url):
    page = BeautifulSoup(get_html_page(url), 'html.parser')
    artist = str(page.findAll('span', itemprop='title')[2].string)
    album_name = str(page.findAll('span', itemprop='title')[3].string)
    song_rows = page.findAll(class_='player-inline')
    songs = [get_song(row) for row in song_rows]

    return {"artist": artist, "album": album_name, "songs": songs}


def get_song(row):
    song = {}
    try:
        song_url = "http://myzuka.club" + str(row.span['data-url'])
        song["url"] = song_url
        song_name = row.find('div', class_='details').p.a.string
    except KeyError:
        song_name = row.find('div', class_='details').p.span.string
    song["name"] = str(song_name)
    song_number = int((row.find('div', class_='position')).string)
    song["number"] = song_number

    return song


def save_song(song, dst_dir):
    response = requests.get(song["url"], stream=True)
    file_name = os.path.join(dst_dir, "{number} {name}.mp3".format(**song))
    with open(file_name, "wb") as f:
        for chunk in response.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
