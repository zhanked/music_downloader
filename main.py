import os
import sys
import time
import threading

from download import get_album_info, save_song

if len(sys.argv) < 2:
    print("Usage: python main.py <url>", file=sys.stderr)
    sys.exit(1)

url = sys.argv[1]
album_info = get_album_info(url)
current_dir = os.path.dirname(os.path.realpath(__file__))
music_dir = "music"
album_dir = os.path.join(current_dir, music_dir,
                         album_info["artist"], album_info["album"])

if not os.path.exists(album_dir):
    os.makedirs(album_dir)

song_list = album_info["songs"][:]


def target(song_list, album_dir):
    while song_list:
        song = song_list.pop(0)
        if 'url' in song:
            print("Getting", song["name"])
            save_song(song, album_dir)
        else:
            print('Song {} is not available. Skipping'.format(song['name']))


start = time.time()
threads = []
for i in range(3):
    t = threading.Thread(target=target, args=(song_list, album_dir))
    threads.append(t)
    threads[i].start()
for t in threads:
    t.join()

print("Took %.2f sec" % (time.time() - start))
